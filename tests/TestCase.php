<?php

namespace Zing\KDNiao\Tests;

use Mockery;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;

class TestCase extends PHPUnitTestCase
{
    public function setUp(): void
    {
        Mockery::globalHelpers();
    }

    public function tearDown(): void
    {
        Mockery::close();
    }
}
