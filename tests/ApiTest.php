<?php

namespace Zing\KDNiao\Tests;

use DateTime;
use Zing\KDNiao\KuaiDiNiao;
use Zing\KDNiao\Models\Commodity;
use Zing\KDNiao\Models\EOrder;
use Zing\KDNiao\Models\Receiver;
use Zing\KDNiao\Models\Sender;
use Zing\KDNiao\Responses\Trace;

class ApiTest extends TestCase
{
    public function config()
    {
        return [
            ['test1452617', 'ca851adf-6e1e-4584-a7d8-338c29a4d628', 'sandbox'],
            ['1452617', '7291bf23-a7d1-4618-802e-130ba368ae8b', 'testing'],
            ['1452617', '7291bf23-a7d1-4618-802e-130ba368ae8b', 'production'],
        ];
    }

    /**
     * @dataProvider config
     *
     * @param mixed $eBusinessID
     * @param mixed $appKey
     * @param mixed $environment
     */
    public function test_get($eBusinessID, $appKey, $environment)
    {
        $kuaiDiNiao = new KuaiDiNiao([
            'EBusinessID' => $eBusinessID,
            'AppKey' => $appKey,
            'environment' => $environment,
        ]);
        $sender = new Sender([
            'Name' => '李先生',
            'Mobile' => '18888888888',
            'ProvinceName' => '广东省',
            'CityName' => '深圳市',
            'ExpAreaName' => '福田区',
            'Address' => '赛格广场5401AB',
        ]);
        $receiver = new Receiver([
            'Name' => '李先生',
            'Mobile' => '18888888888',
            'ProvinceName' => '广东省',
            'CityName' => '深圳市',
            'ExpAreaName' => '福田区',
            'Address' => '赛格广场5401AB',
        ]);
        $eOrder = new EOrder([
            'ShipperCode' => 'SF',
            'OrderCode' => date('YmdHis'),
            'PayType' => 1,
            'ExpType' => 1,
            'Callback' => 'http://erp.beta.jojotu.cn/api/kdniao/callback',
            'IsReturnPrintTemplate' => 0,
            'TemplateSize' => 180,
        ]);
        $eOrder->setSender($sender);
        $eOrder->setReceiver($receiver);
        $commodity = new Commodity([
            'GoodsName' => '其他',
        ]);
        $eOrder->addCommodity($commodity);

        $response = $kuaiDiNiao->eOrderService($eOrder);
        self::assertEquals($eBusinessID, $response->getEBusinessID());
        self::assertTrue($response->isSuccess(), $response->getReason());
        $order = $response->getOrder();
        $response = $kuaiDiNiao->query(
            $order->getOrderCode(),
            $order->getShipperCode(),
            $order->getLogisticCode()
        );
        self::assertEquals($eBusinessID, $response->getEBusinessID());
        self::assertTrue($response->isSuccess(), $response->getReason());
        if ($response->getOrderCode()) {
            self::assertEquals($order->getOrderCode(), $response->getOrderCode());
        }
        self::assertEquals($order->getShipperCode(), $response->getShipperCode());
        self::assertEquals($order->getLogisticCode(), $response->getLogisticCode());
        foreach ($response->getTraces() as $trace) {
            self::assertInstanceOf(Trace::class, $trace);
        }
        $eOrder->setAttribute('LogisticCode', $order->getLogisticCode());
        $response = $kuaiDiNiao->subscribe($eOrder);
        self::assertEquals($eBusinessID, $response->getEBusinessID());
        self::assertTrue($response->isSuccess(), $response->getReason());
        self::assertTrue(DateTime::createFromFormat('!Y-m-d H:i:s', $response->getUpdateTime())->format('Y-m-d H:i:s') === $response->getUpdateTime());
        if ($response->getEstimatedDeliveryTime()) {
            self::assertTrue(DateTime::createFromFormat('!Y-m-d H:i:s', $response->getEstimatedDeliveryTime())->format('Y-m-d H:i:s') === $response->getEstimatedDeliveryTime());
        }
        if ($kuaiDiNiao->isProduction()) {
            $response = $kuaiDiNiao->cancelEOrderService(
                $order->getOrderCode(),
                $order->getShipperCode(),
                $order->getLogisticCode()
            );
            self::assertTrue($response->isSuccess(), $response->getReason());
        }
    }

    /**
     * @dataProvider config
     *
     * @param mixed $eBusinessID
     * @param mixed $appKey
     * @param mixed $environment
     */
    public function test_handle($eBusinessID, $appKey, $environment)
    {
        $data = [
            'DataSign' => 'OGVkNDRiODY1YWI1MTZlN2Q4ODgwZjg2YTk4YWQwOTg=',
            'RequestType' => '101',
            'RequestData' => '{"PushTime":"2019-08-06 19:09:39","EBusinessID":"test1452617","Data":[{"LogisticCode":"1234561","ShipperCode":"SF","Traces":[{"AcceptStation":"顺丰速运已收取快件","AcceptTime":"2019-08-06 19:09:39","Remark":""},{"AcceptStation":"货物已经到达深圳","AcceptTime":"2019-08-06 19:09:392","Remark":""},{"AcceptStation":"货物到达福田保税区网点","AcceptTime":"2019-08-06 19:09:393","Remark":""},{"AcceptStation":"货物已经被张三签收了","AcceptTime":"2019-08-06 19:09:394","Remark":""}],"State":"3","EBusinessID":"test1452617","Success":true,"Reason":"","CallBack":"","EstimatedDeliveryTime":"2019-08-06 19:09:39"}],"Count":"1"}',
        ];
        $kuaiDiNiao = (new KuaiDiNiao([
            'EBusinessID' => $eBusinessID,
            'AppKey' => $appKey,
            'environment' => $environment,
        ]));
        if ($kuaiDiNiao->isSandbox()) {
            $kuaiDiNiao->handle($data, function ($data) {
                $this->assertTrue(true);
            });
        } else {
            self::assertTrue(true);
        }
    }

    /**
     * @dataProvider config
     *
     * @param $eBusinessID
     * @param $appKey
     * @param $environment
     */
    public function test_m($eBusinessID, $appKey, $environment)
    {
        $kuaiDiNiao = (new KuaiDiNiao([
            'EBusinessID' => $eBusinessID,
            'AppKey' => $appKey,
            'environment' => $environment,
        ]));
        $kuaiDiNiao->multiplePrint('[{"OrderCode":"234351215333113311353","PortName":"打印机名称一"},{"OrderCode":"234351215333113311354","PortName":"打印机名称二"}]');
    }
}
