<?php

namespace Zing\KDNiao\Tests;

use Zing\KDNiao\Models\Commodity;
use Zing\KDNiao\Models\EOrder;
use Zing\KDNiao\Models\Receiver;
use Zing\KDNiao\Models\Sender;

class EOrderTest extends TestCase
{
    public function test_json()
    {
        $sender = new Sender([
            'Name' => '李先生',
            'Mobile' => '18888888888',
            'ProvinceName' => '广东省',
            'CityName' => '深圳市',
            'ExpAreaName' => '福田区',
            'Address' => '赛格广场5401AB',
        ]);
        $receiver = new Receiver([
            'Name' => '李先生',
            'Mobile' => '18888888888',
            'ProvinceName' => '广东省',
            'CityName' => '深圳市',
            'ExpAreaName' => '福田区',
            'Address' => '赛格广场5401AB',
        ]);
        $order = new EOrder([
            'ShipperCode' => 'SF',
            'OrderCode' => date('YmdHis'),
            'PayType' => 1,
            'ExpType' => 1,
            'Callback' => 'http://erp.beta.jojotu.cn/api/kdniao/callback',
            'IsReturnPrintTemplate' => 1,
            'TemplateSize' => 180,
        ]);
        $order->setSender($sender);
        $order->setReceiver($receiver);

        $order->addCommodity(new Commodity([
            'GoodsName' => '其他',
        ]));
    }
}
