<?php

return (new \PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@PhpCsFixer' => true,
            'phpdoc_summary' => false,
            'phpdoc_to_comment' => false,
            'phpdoc_no_empty_return' => false,
            'phpdoc_no_alias_tag' => false,
            'concat_space' => ['spacing' => 'one'],
            'phpdoc_align' => ['align' => 'left'],
            'array_syntax' => ['syntax' => 'short'],
            'yoda_style' => false,
            'class_attributes_separation' => true,
            'visibility_required' => ['elements' => ['property', 'method', 'const']],
            'ordered_imports' => ['imports_order' => ['class', 'function', 'const']],
            'not_operator_with_successor_space' => true,
            'ordered_class_elements' => false,
            'phpdoc_types_order' => false,
            'multiline_whitespace_before_semicolons' => true,
            'php_unit_test_class_requires_covers' => false,
            'php_unit_internal_class' => false,
            'php_unit_method_casing' => ['case' => 'snake_case'],
            'array_indentation' => false,
        ]
    )
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__)
            ->exclude(__DIR__ . '/vendor')
    );

