<?php

namespace Zing\KDNiao\Messages;

use Zing\KDNiao\Models\Model;

class TraceChanged extends Model
{
    public function getPushTime()
    {
        return $this->getAttribute('PushTime');
    }

    public function getEBusinessID()
    {
        return $this->getAttribute('EBusinessID');
    }

    public function getCount()
    {
        return $this->getAttribute('Count');
    }

    public function getData()
    {
        return collect($this->getAttribute('Data', []))->map(function ($item) {
            return new Order($item);
        });
    }
}
