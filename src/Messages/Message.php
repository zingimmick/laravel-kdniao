<?php

namespace Zing\KDNiao\Messages;

use Zing\KDNiao\Concerns\HasAttributes;
use Zing\KDNiao\RequestType;

class Message
{
    use HasAttributes;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    public function getDataSign()
    {
        return $this->getAttribute('DataSign');
    }

    public function getRequestType()
    {
        return $this->getAttribute('RequestType');
    }

    public function getRequestData()
    {
        $data = json_decode($this->getAttribute('RequestData'), true);

        switch ($this->getRequestType()) {
            case RequestType::TRACE_CHANGED:
                $data = new TraceChanged($data);

                break;

            case RequestType::CUSTOMER_AUDITED:
                break;
        }

        return $data;
    }
}
