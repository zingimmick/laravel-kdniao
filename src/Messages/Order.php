<?php

namespace Zing\KDNiao\Messages;

use Zing\KDNiao\Models\Model;
use Zing\KDNiao\Responses\Trace;

/**
 * Class Order
 */
class Order extends Model
{
    public function getLogisticCode()
    {
        $this->getAttribute('LogisticCode');
    }

    public function getShipperCode()
    {
        $this->getAttribute('ShipperCode');
    }

    public function getTraces()
    {
        return collect($this->getAttribute('Traces', []))->map(function ($item) {
            return new Trace($item);
        });
    }

    public function getState()
    {
        return $this->getAttribute('State');
    }

    public function getEBusinessID()
    {
        return $this->getAttribute('EBusinessID');
    }

    public function getSuccess()
    {
        return $this->getAttribute('Success');
    }

    public function getReason()
    {
        return $this->getAttribute('Reason');
    }

    public function getCallBack()
    {
        return $this->getAttribute('CallBack');
    }

    public function getEstimatedDeliveryTime()
    {
        return $this->getAttribute('EstimatedDeliveryTime');
    }
}
