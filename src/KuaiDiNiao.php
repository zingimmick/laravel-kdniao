<?php

namespace Zing\KDNiao;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Zing\KDNiao\Requests\BaseRequest;
use Zing\KDNiao\Requests\EorderserviceCancelRequest;
use Zing\KDNiao\Requests\EorderserviceRequest;
use Zing\KDNiao\Requests\MultiplePrintRequest;
use Zing\KDNiao\Requests\Query;
use Zing\KDNiao\Requests\Subscribe;
use Zing\KDNiao\Responses\EOrderServiceResponse;
use Zing\KDNiao\Responses\QueryResponse;
use Zing\KDNiao\Responses\SubscribeResponse;

class KuaiDiNiao
{
    private $config;

    protected $client;

    protected $endPoint = [
        'sandbox' => 'http://sandboxapi.kdniao.com:8080/kdniaosandbox/gateway/exterfaceInvoke.json',
        'testing' => 'http://testapi.kdniao.com:8081',
        'production' => 'http://api.kdniao.com',
    ];

    /**
     * KuaiDiNiao constructor.
     *
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = collect($config);
        $this->client = new Client();
    }

    /**
     * @param $data
     *
     * @return EOrderServiceResponse
     */
    public function eOrderService($data)
    {
        return $this->send(new EorderserviceRequest($data));
    }

    public function getEnvironment()
    {
        return $this->config->get('environment', 'production');
    }

    public function isSandbox()
    {
        return $this->getEnvironment() === 'sandbox';
    }

    public function isProduction()
    {
        return $this->getEnvironment() === 'production';
    }

    public function getEndPoint()
    {
        return $this->endPoint[$this->getEnvironment()];
    }

    public function getAppKey()
    {
        return $this->config->get('AppKey');
    }

    public function getEBusinessID()
    {
        return $this->config->get('EBusinessID');
    }

    public function send(BaseRequest $request)
    {
        $data = [
            'EBusinessID' => $this->getEBusinessID(),
            'RequestType' => $request->getRequestType(),
            'RequestData' => urlencode($request->getSignData()),
            'DataType' => DataType::JSON,
        ];
        $data['DataSign'] = $this->encrypt($request->getSignData(), $this->getAppKey());
        $data = json_decode($this->sendPost($this->getUrl($request->getUri()), $data), true);

        return $request->toResponse($data);
    }

    public function getUrl($uri)
    {
        return $this->isSandbox() ? $this->getEndPoint() : ($this->getEndPoint() . $uri);
    }

    public function sendPost($url, $datas)
    {
        $response = $this->client->post($url, [
            'form_params' => $datas,
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * 电商Sign签名生成
     *
     * @param string $data 内容
     * @param mixed $app_key AppKey
     *
     * @return string DataSign签名
     */
    public function encrypt($data, $app_key)
    {
        return base64_encode(md5($data . $app_key));
    }

    /**
     * 使用特定function对数组中所有元素做处理
     *
     * @param array &$array 要处理的字符串
     * @param string $function 要执行的函数
     * @param bool $apply_to_keys_also 是否也应用到key上
     */
    public function arrayRecursive(&$array, $function, $apply_to_keys_also = false)
    {
        static $recursive_counter = 0;
        if (++$recursive_counter > 1000) {
            exit('possible deep recursion attack');
        }
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $this->arrayRecursive($array[$key], $function, $apply_to_keys_also);
            } else {
                $array[$key] = $function($value);
            }

            if ($apply_to_keys_also && is_string($key)) {
                $new_key = $function($key);
                if ($new_key != $key) {
                    $array[$new_key] = $array[$key];
                    unset($array[$key]);
                }
            }
        }
        --$recursive_counter;
    }

    /**
     * 将数组转换为JSON字符串（兼容中文）
     *
     * @param array $array 要转换的数组
     *
     * @return string 转换得到的json字符串
     */
    public function JSON($array)
    {
        $this->arrayRecursive($array, 'urlencode', true);
        $json = json_encode($array);

        return urldecode($json);
    }

    public function expresses()
    {
        $expresses = '[{"code": "AJ","name": "安捷快递"},{"code": "A`MAZON","name": "亚马逊物流"},{"code": "ANE","name": "安能物流"},{"code": "AXD","name": "安信达快递"},{"code": "AYCA","name": "澳邮专线"},{"code": "BFDF","name": "百福东方"},{"code": "BQXHM","name": "北青小红帽"},{"code": "BTWL","name": "百世快运"},{"code": "CCES","name": "CCES快递"},{"code": "CDSTKY","name": "成都善途速运"},{"code": "CITY100","name": "城市100"},{"code": "CJKD","name": "城际快递"},{"code": "CNPEX","name": "CNPEX中邮快递"},{"code": "COE","name": "COE东方快递"},{"code": "CSCY","name": "长沙创一"},{"code": "DBL","name": "德邦"},{"code": "DSWL","name": "D速物流"},{"code": "DTWL","name": "大田物流"},{"code": "EMS","name": "EMS"},{"code": "FAST","name": "快捷速递"},{"code": "FEDEX","name": "FEDEX联邦(国内件）"},{"code": "FEDEX_GJ","name": "FEDEX联邦(国际件）"},{"code": "FKD","name": "飞康达"},{"code": "GDEMS","name": "广东邮政"},{"code": "GSD","name": "共速达"},{"code": "GTO","name": "国通快递"},{"code": "GTSD","name": "高铁速递"},{"code": "HFWL","name": "汇丰物流"},{"code": "HHTT","name": "天天快递"},{"code": "HLWL","name": "恒路物流"},{"code": "HOAU","name": "天地华宇"},{"code": "HOTSCM","name": "鸿桥供应链"},{"code": "HPTEX","name": "海派通物流公司"},{"code": "hq568","name": "华强物流"},{"code": "HTKY","name": "百世快递"},{"code": "HXLWL","name": "华夏龙物流"},{"code": "HYLSD","name": "好来运快递"},{"code": "JGSD","name": "京广速递"},{"code": "JIUYE","name": "九曳供应链"},{"code": "JJKY","name": "佳吉快运"},{"code": "JLDT","name": "嘉里物流"},{"code": "JTKD","name": "捷特快递"},{"code": "JXD","name": "急先达"},{"code": "JYKD","name": "晋越快递"},{"code": "JYM","name": "加运美"},{"code": "JYWL","name": "佳怡物流"},{"code": "KYWL","name": "跨越物流"},{"code": "LB","name": "龙邦快递"},{"code": "LHT","name": "联昊通速递"},{"code": "MHKD","name": "民航快递"},{"code": "MLWL","name": "明亮物流"},{"code": "NEDA","name": "能达速递"},{"code": "PADTF","name": "平安达腾飞快递"},{"code": "PANEX","name": "泛捷快递"},{"code": "PCA","name": "PCA Express"},{"code": "QCKD","name": "全晨快递"},{"code": "QFKD","name": "全峰快递"},{"code": "QRT","name": "全日通快递"},{"code": "QUICK","name": "快客快递"},{"code": "RFD","name": "如风达"},{"code": "RFEX","name": "瑞丰速递"},{"code": "SAD","name": "赛澳递"},{"code": "SAWL","name": "圣安物流"},{"code": "SBWL","name": "盛邦物流"},{"code": "SDWL","name": "上大物流"},{"code": "SF","name": "顺丰快递"},{"code": "SFWL","name": "盛丰物流"},{"code": "SHWL","name": "盛辉物流"},{"code": "ST","name": "速通物流"},{"code": "STO","name": "申通快递"},{"code": "STWL","name": "速腾快递"},{"code": "SUBIDA","name": "速必达物流"},{"code": "SURE","name": "速尔快递"},{"code": "TSSTO","name": "唐山申通"},{"code": "UAPEX","name": "全一快递"},{"code": "UC","name": "优速快递"},{"code": "UEQ","name": "UEQ Express"},{"code": "WJWL","name": "万家物流"},{"code": "WXWL","name": "万象物流"},{"code": "XBWL","name": "新邦物流"},{"code": "XFEX","name": "信丰快递"},{"code": "XJ","name": "新杰物流"},{"code": "XYT","name": "希优特"},{"code": "YADEX","name": "源安达快递"},{"code": "YCWL","name": "远成物流"},{"code": "YD","name": "韵达快递"},{"code": "YDH","name": "义达国际物流"},{"code": "YFEX","name": "越丰物流"},{"code": "YFHEX","name": "原飞航物流"},{"code": "YFSD","name": "亚风快递"},{"code": "YTKD","name": "运通快递"},{"code": "YTO","name": "圆通速递"},{"code": "YXKD","name": "亿翔快递"},{"code": "YZPY","name": "邮政平邮/小包"},{"code": "ZENY","name": "增益快递"},{"code": "ZHQKD","name": "汇强快递"},{"code": "ZJS","name": "宅急送"},{"code": "ZTE","name": "众通快递"},{"code": "ZTKY","name": "中铁快运"},{"code": "ZTO","name": "中通速递"},{"code": "ZTWL","name": "中铁物流"},{"code": "ZYWL","name": "中邮物流"}]';

        return json_decode($expresses, true);
    }

    public function getExpressByCode($code)
    {
        if ($code) {
            $expresses = self::expresses();
            $express = array_filter($expresses, function ($express) use ($code) {
                return $express['code'] == $code;
            });
            if ($express) {
                return current($express);
            }

            return null;
        }

        return null;
    }

    /**
     * @param $orderSn
     * @param $expressCode
     * @param $expressSn
     *
     * @return QueryResponse
     */
    public function query($orderSn, $expressCode, $expressSn)
    {
        $requestData = [
            'OrderCode' => $orderSn,
            'ShipperCode' => $expressCode,
            'LogisticCode' => $expressSn,
        ];

        return $this->send(new Query($requestData));
    }

    /**
     * @param $order
     *
     * @return SubscribeResponse
     */
    public function subscribe($order)
    {
        return $this->send(new Subscribe($order));
    }

    public function check($value, $signedValue)
    {
        return $this->encrypt($value, $this->getAppKey()) === $signedValue;
    }

    public function handle($data, $success, $fail = null)
    {
        if (Arr::has($data, ['RequestData', 'DataSign']) && $this->check($data['RequestData'], $data['DataSign'])) {
            return $success($data);
        }

        return $fail ? ($data) : null;
    }

    public function cancelEOrderService($orderSn, $expressCode, $expressSn)
    {
        $requestData = [
            'OrderCode' => $orderSn,
            'ShipperCode' => $expressCode,
            'ExpNo' => $expressSn,
        ];

        return $this->send(new EorderserviceCancelRequest($requestData));
    }

    public function multiplePrint($data)
    {
        return $this->send(new MultiplePrintRequest($data));
    }
}
