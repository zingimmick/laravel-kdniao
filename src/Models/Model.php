<?php

namespace Zing\KDNiao\Models;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Zing\KDNiao\Concerns\HasAttributes;

abstract class Model implements Arrayable, \JsonSerializable, Jsonable
{
    use HasAttributes;

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    public function toArray()
    {
        foreach ($this->attributes as $key => $value) {
            if ($value instanceof Arrayable) {
                $this->attributes[$key] = $value->toArray();
            }
        }

        return $this->attributes;
    }

    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }
}
