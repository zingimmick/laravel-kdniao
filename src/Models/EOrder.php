<?php

namespace Zing\KDNiao\Models;

class EOrder extends Model
{
    public function setSender(Sender $sender): void
    {
        $this->attributes['Sender'] = $sender;
    }

    public function setReceiver(Receiver $receiver): void
    {
        $this->attributes['Receiver'] = $receiver;
    }

    public function addCommodity(Commodity $commodity)
    {
        $this->attributes['Commodity'][] = $commodity;
    }
}
