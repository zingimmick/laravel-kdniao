<?php

namespace Zing\KDNiao\Models;

/**
 * Class Person
 *
 * @property string $Name
 * @property string $Mobile
 * @property string $ProvinceName
 * @property string $CityName
 * @property string $ExpAreaName
 * @property string $Address
 */
class Person extends Model
{
}
