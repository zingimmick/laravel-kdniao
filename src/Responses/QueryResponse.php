<?php

namespace Zing\KDNiao\Responses;

class QueryResponse extends BaseResponse
{
    public function getOrderCode()
    {
        return $this->getAttribute('OrderCode');
    }

    public function getShipperCode()
    {
        return $this->getAttribute('ShipperCode');
    }

    public function getLogisticCode()
    {
        return $this->getAttribute('LogisticCode');
    }

    public function getState()
    {
        return $this->getAttribute('State');
    }

    public function getTraces()
    {
        return collect($this->getAttribute('Traces', []))->map(function ($item) {
            return new Trace($item);
        });
    }
}
