<?php

namespace Zing\KDNiao\Responses;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Zing\KDNiao\Concerns\HasAttributes;

class BaseResponse implements Arrayable, \JsonSerializable, Jsonable
{
    use HasAttributes;

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    public function toArray()
    {
        foreach ($this->attributes as $key => $value) {
            if ($value instanceof Arrayable) {
                $this->attributes[$key] = $value->toArray();
            }
        }

        return $this->attributes;
    }

    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function getSuccess()
    {
        return $this->getAttribute('Success');
    }

    public function isSuccess()
    {
        return $this->getSuccess() === true;
    }

    public function getResultCode()
    {
        return $this->getAttribute('ResultCode');
    }

    public function getReason()
    {
        return $this->getAttribute('Reason') ?: '';
    }

    public function getEBusinessID()
    {
        return $this->getAttribute('EBusinessID');
    }
}
