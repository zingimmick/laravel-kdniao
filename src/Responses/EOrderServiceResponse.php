<?php

namespace Zing\KDNiao\Responses;

class EOrderServiceResponse extends BaseResponse
{
    public function getOrder()
    {
        return new Order($this->getAttribute('Order'));
    }
}
