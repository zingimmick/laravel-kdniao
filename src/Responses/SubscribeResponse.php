<?php

namespace Zing\KDNiao\Responses;

class SubscribeResponse extends BaseResponse
{
    public function getUpdateTime()
    {
        return $this->getAttribute('UpdateTime');
    }

    public function getEstimatedDeliveryTime()
    {
        return $this->getAttribute('EstimatedDeliveryTime');
    }
}
