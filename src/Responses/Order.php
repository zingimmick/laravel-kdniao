<?php

namespace Zing\KDNiao\Responses;

use Zing\KDNiao\Models\Model;

class Order extends Model
{
    public function getOrderCode()
    {
        return $this->getAttribute('OrderCode', '');
    }

    public function getShipperCode()
    {
        return $this->getAttribute('ShipperCode');
    }

    public function getLogisticCode()
    {
        return $this->getAttribute('LogisticCode');
    }
}
