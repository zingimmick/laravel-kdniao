<?php

namespace Zing\KDNiao\Responses;

use Zing\KDNiao\Models\Model;

class Trace extends Model
{
    public function getAcceptTime()
    {
        return $this->getAttribute('AcceptTime');
    }

    public function getAcceptStation()
    {
        return $this->getAttribute('AcceptStation');
    }

    public function getRemark()
    {
        return $this->getAttribute('Remark');
    }
}
