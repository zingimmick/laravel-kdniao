<?php

namespace Zing\KDNiao\Requests;

use Zing\KDNiao\RequestType;
use Zing\KDNiao\Responses\SubscribeResponse;

class Subscribe extends BaseRequest
{
    protected $requestType = RequestType::SUBSCRIBE;

    protected $uri = '/api/dist';

    public function toResponse($data)
    {
        return new SubscribeResponse($data);
    }
}
