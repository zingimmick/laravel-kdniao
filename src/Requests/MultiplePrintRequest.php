<?php

namespace Zing\KDNiao\Requests;

class MultiplePrintRequest extends BaseRequest
{
    protected $uri = '/External/PrintOrder.aspx';
}
