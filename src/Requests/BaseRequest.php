<?php

namespace Zing\KDNiao\Requests;

use Zing\KDNiao\Responses\BaseResponse;

class BaseRequest
{
    protected $data = [];

    protected $uri = '';

    protected $requestType = '';

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getRequestType(): string
    {
        return $this->requestType;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * BaseRequest constructor.
     *
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getSignData()
    {
        return json_encode($this->data, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param array $data
     *
     * @return BaseResponse
     */
    public function toResponse($data)
    {
        return new BaseResponse($data);
    }
}
