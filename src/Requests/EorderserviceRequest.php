<?php

namespace Zing\KDNiao\Requests;

use Zing\KDNiao\RequestType;
use Zing\KDNiao\Responses\BaseResponse;
use Zing\KDNiao\Responses\EOrderServiceResponse;

class EorderserviceRequest extends BaseRequest
{
    protected $uri = '/api/EOrderService';

    protected $requestType = RequestType::EORDER;

    /**
     * @param array $data
     *
     * @return BaseResponse|EOrderServiceResponse
     */
    public function toResponse($data)
    {
        return new EOrderServiceResponse($data);
    }
}
