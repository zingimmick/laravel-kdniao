<?php

namespace Zing\KDNiao\Requests;

use Zing\KDNiao\RequestType;

class EorderserviceCancelRequest extends BaseRequest
{
    protected $uri = '/api/EOrderService';

    protected $requestType = RequestType::EORDER_CANCEL;

//    public function toResponse($data)
//    {
//        // TODO: Implement toResponse() method.
//    }
}
