<?php

namespace Zing\KDNiao\Requests;

use Zing\KDNiao\RequestType;
use Zing\KDNiao\Responses\QueryResponse;

class Query extends BaseRequest
{
    protected $requestType = RequestType::QUERY;

    protected $uri = '/Ebusiness/EbusinessOrderHandle.aspx';

    public function toResponse($data)
    {
        return new QueryResponse($data);
    }
}
