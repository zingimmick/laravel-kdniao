<?php

namespace Zing\KDNiao;

class RequestType
{
    public const CUSTOMER_AUDITED = 1117;

    public const TRACE_CHANGED = 101;

    public const EORDER = 1007;

    public const EORDER_CANCEL = 1147;

    public const QUERY = 1002;

    public const SUBSCRIBE = 1008;
}
